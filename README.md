# Back - End

Back - End tools
- nodejs
- typescript
- express
- mongo

Description: 

Scenario: Secure Saver
Problem Description:

A small business company has a manual process to register their employees' laptops, maintaining a plain text document with the brand, model and serial number. They would like to automate this process and also keep a record adding the employee's email.
A Solutions Architect has recommended creating a PoC (Proof of Concept) where the main objective will be to create a web application to maintain these records. The information should be shown encrypted in the UI for security purposes, if you want to see a record, you have to click a
button to decrypt the information.
Each laptop’s information must be persisted in a database, this task will be performed by a backend API.
A separate code must be in charge of encrypting the data before it is stored and decrypting it on demand when clicking the button.

The SA has suggested to divide the full solution in three parts as follows:
    Part I - Back-End Core:
        Create a solution to maintain the records (CRUD). The application must be able to store
        and retrieve the data. It must expose the endpoints using standard Http methods.
    Part II - Front-End Site:
        Create a basic UI (User Interface) using ngx-admin template with the required fields in
        order to list, store, change and remove records.
    Part III - Encryption Service:
        Create a separated generic service with encryption/decryption capabilities. It should be
        agnostic, as it could be later used to encrypt and decrypt data for other applications.
