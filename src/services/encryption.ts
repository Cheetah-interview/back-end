import crypto from 'crypto';

interface IConfig {
    algorithm?: string;
    encryptionKey?: string;
    salt?: string;
    iv?: Buffer;
}

export class Encryption {

    static algorithm: string = 'aes-192-cbc';
    static key: Buffer = crypto.scryptSync('SUPERSECRETEKEY', 'salt', 24);
    static iv: Buffer = crypto.randomBytes(16);


    static encrypt = (data: string): string => {

        const cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);
        const encryptedData = cipher.update(data, 'utf8', 'hex') +cipher.final('hex');

        return encryptedData;
    }

    static decrypt = (data: string) : string => {

        const decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);
        const decryptedData = decipher.update(data, 'hex', 'utf8') + decipher.final('utf8');

        return decryptedData;
    }

}