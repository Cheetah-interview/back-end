import express, {Request, Response} from 'express';
import { EmployeeLaptop } from '../models/EmployeeLaptop';

const router = express.Router();


router.delete('/api/employeeLaptops/:id', async(req: Request, res: Response) => {
    const { id } = req.params;

    if (!id) {
        return res.status(400).send('the ID is required!');
    }

    const record = await EmployeeLaptop.findByIdAndDelete(id);

    if (!record){
        return res.status(204).send('Document dont found!')
    }

    res.status(200).send(record);
    
});

export { router as deleteRouter };