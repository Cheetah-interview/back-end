import express, {Request, Response} from 'express';
import { body, validationResult } from 'express-validator';
import { EmployeeLaptop } from '../models/EmployeeLaptop';
import { Encryption } from '../services/encryption';

const router = express.Router();


router.put('/api/employeeLaptops/:id', [
    body('brand')
        .notEmpty(),
    body('model')
        .notEmpty(),
    body('serial')
        .notEmpty(),
    body('email')
        .notEmpty()
], async (req: Request, res: Response) => {
    const errors = validationResult(req);
    const id = req.params.id;
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    if (!id){
        return res.status(400).send('ID doesnt exist!');
    }

    if (!errors.isEmpty()) {
        return res.status(400).send(errors.array());
    }
    const { brand, model, serial, email } = req.body;
    let encryptedEmail = email;
    // if there is a new email
    if (emailRegexp.test(email)){
        encryptedEmail = Encryption.encrypt(email);
    }
    

    const existingRecord = await EmployeeLaptop.findById(id);

    if (!existingRecord) {
        return res.status(400).send('Record dont found!');
    }

    existingRecord.brand = brand;
    existingRecord.model = model;
    existingRecord.serial = serial;
    existingRecord.email = encryptedEmail;

    existingRecord.save();

    res.status(200).send(existingRecord);

});

export { router as updateRouter };