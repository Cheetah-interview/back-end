import express, {Request, Response} from 'express';
import { EmployeeLaptop } from '../models/EmployeeLaptop';
import { Encryption } from '../services/encryption';

const router = express.Router();


router.get('/api/employeeLaptops/:id', async (req: Request, res: Response) => {
    const id = req.params.id;

    console.log("id: ", id)

    if (!id) {
        return res.status(400).send('ID doesnt exist!');
    }

    const existingRecord = await EmployeeLaptop.findById(id);

    console.log("record: ", existingRecord)

    if (!existingRecord) {
        return res.status(400).send('Document dont found!');
    }

    const decryptedEmail = Encryption.decrypt(existingRecord.email);
    console.log("decrypt: ", decryptedEmail);
    const decryptedData = {...existingRecord._doc, email: decryptedEmail };
    return res.status(200).send(decryptedData);
    
}); 

router.get('/api/employeeLaptops', async(req: Request, res: Response) => {
    const records = await EmployeeLaptop.find({});

    res.status(200).send(records);
});

export { router as showRouter };