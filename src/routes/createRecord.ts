import express, {Request, Response} from 'express';
import { body, validationResult } from 'express-validator';
import { EmployeeLaptop } from '../models/EmployeeLaptop';
import { Encryption } from '../services/encryption';

const router = express.Router();


router.post('/api/employeeLaptops/create', [
    body('brand')
        .notEmpty(),
    body('model')
        .notEmpty(),
    body('serial')
        .notEmpty(),
    body('email')
        .isEmail()
        .withMessage('Email must be valid')
], async(req: Request, res: Response) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).send(errors.array());
    }

    const { brand, model, serial, email } = req.body;
    const encryptedEmail = Encryption.encrypt(email);

    const newRecord = EmployeeLaptop.build({brand, model, serial, email:encryptedEmail});
    await newRecord.save();
    
    res.status(201).send(newRecord);

});

export { router as createRouter };