import mongoose from 'mongoose';
//import Encryption from '../services/encryption'

interface EmployeeLaptopAttrs {
    brand: string;
    model: string;
    serial: string;
    email: string;
}

interface EmployeeLaptopModel extends mongoose.Model<any> {
    build(attrs: EmployeeLaptopAttrs): EmployeeLaptopDoc;
}

interface EmployeeLaptopDoc extends mongoose.Document {
    brand: string;
    model: string;
    serial: string;
    email: string;
}

const EmployeeLaptopSchema = new mongoose.Schema({
    brand:{
        type: String,
        required: true
    },
    model:{
        type: String,
        required: true
    },
    serial:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    }
});
/*
EmployeeLaptopSchema.pre('save', async function(done){
    if(this.isModified('password')) {
        //
    }
} )
*/

EmployeeLaptopSchema.statics.build = (attrs: EmployeeLaptopAttrs) => {
    return new EmployeeLaptop(attrs);
};

const EmployeeLaptop = mongoose.model<EmployeeLaptopDoc, EmployeeLaptopModel>('EmployeeLaptop', EmployeeLaptopSchema);


export { EmployeeLaptop };